import React, { useState, useEffect } from "react";
import { CreateObject } from "features/create-object";

export const CreateObjectWidget = () => {
  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);

  useEffect(() => {
    function handleResize() {
      setWidth(window.innerWidth);
      setHeight(window.innerHeight);
    }
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className="App">
      <CreateObject
        width={width}
        height={height}
        cameraPosition={{ x: 0, y: 0, z: 5 }}
      />
    </div>
  );
};
