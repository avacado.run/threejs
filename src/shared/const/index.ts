export const sceneColor = "#141414";

export const lightColor = 0xffffff;

export const materialColor = "#ffc53d";

export const intensity = 1;

export const iPos = [2, 2, 2];

export const dPos = [3, 1, 3];

export const tPos = [4, 3, 2];
