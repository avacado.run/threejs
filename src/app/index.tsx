import { CreateObjectWidget } from "widgets";
import { ConfigProvider, theme } from "antd";

const App = () => (
  <ConfigProvider
    theme={{
      algorithm: theme.darkAlgorithm,
      token: {
        colorPrimary: "#722ed1",
      },
    }}
  >
    <div className="App">
      <CreateObjectWidget />
    </div>
  </ConfigProvider>
);

export default App;
