export interface CreateObjectsProps {
  cameraPosition: { x: number; y: number; z: number };
  width: number;
  height: number;
}
