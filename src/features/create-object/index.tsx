import { useCreateObject } from "./hooks";
import { CreateObjectsProps } from "./types";
import { Button, Select, Space, Switch, Typography } from "antd";

export const CreateObject = ({
  cameraPosition,
  width,
  height,
}: CreateObjectsProps) => {
  const {
    mountRef,
    handleMouseMove,
    handleMouseDown,
    handleMouseUp,
    handleMouseWheel,
    handleTetrahedronClick,
    handleHexahedronClick,
    handleOctahedronClick,
    handleChangeView,
    viewAxis,
    setViewAxis,
  } = useCreateObject({ cameraPosition, width, height });
  return (
    <div style={{ width: "100vw", height: "100vh" }}>
      <div
        style={{
          position: "absolute",
          display: "flex",
          flexDirection: "column",
          padding: "24px",
        }}
      >
        <Space direction="vertical">
          <Typography.Text>Выберите фигуру:</Typography.Text>
          <Space wrap>
            <Button type="primary" onClick={handleTetrahedronClick}>
              Тетраэдр
            </Button>
            <Button type="primary" onClick={handleHexahedronClick}>
              Гексаэдр
            </Button>
            <Button type="primary" onClick={handleOctahedronClick}>
              Октаэдр
            </Button>
          </Space>
          <Typography.Text>Выберите режим рендера:</Typography.Text>
          <Space wrap>
            <Select
              defaultValue="perspective"
              style={{ width: 240 }}
              onChange={handleChangeView}
              options={[
                { value: "perspective", label: "Перспективный" },
                { value: "isometric", label: "Изометрическая проекция" },
                { value: "dimetric", label: "Диметрическая проекция" },
                { value: "trimetric", label: "Триметрическая проекция" },
              ]}
            />
          </Space>
          <Typography.Text>Оси координат:</Typography.Text>
          <Space direction="vertical">
            <Switch
              onChange={setViewAxis}
              checkedChildren="Выкл"
              unCheckedChildren="Вкл"
              checked={viewAxis}
            />
          </Space>
        </Space>
      </div>

      <div
        style={{ width: "100%", height: "100%" }}
        ref={mountRef}
        onMouseMove={handleMouseMove}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
        onWheel={handleMouseWheel}
      />
    </div>
  );
};
