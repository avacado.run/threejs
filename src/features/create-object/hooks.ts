import React, { useEffect, useRef, useState } from "react";
import * as THREE from "three";
import {
  lightColor,
  dPos,
  intensity,
  iPos,
  materialColor,
  tPos,
  sceneColor,
} from "shared/const";
import { CreateObjectsProps } from "./types";

export const useCreateObject = ({
  cameraPosition,
  width,
  height,
}: CreateObjectsProps) => {
  const mountRef = useRef<HTMLDivElement>(null);
  const [geometry, setGeometry] = useState<any>(
    new THREE.TetrahedronGeometry(1)
  );
  let mouseX = 0;
  let mouseY = 0;
  let isMouseDown = false;
  const cooff = width < 900 ? 500 : 1000;
  const [view, setView] = useState("perspective");
  const [viewAxis, setViewAxis] = useState(false);
  const [camera] = useState<THREE.PerspectiveCamera>(
    new THREE.PerspectiveCamera(55, width / height, 0.4, 1000)
  );
  const [orthographicCamera] = useState(
    new THREE.OrthographicCamera(
      -width / cooff,
      width / cooff,
      height / cooff,
      -height / cooff,
      1,
      1000
    )
  );

  useEffect(() => {
    const width = mountRef.current!.clientWidth;
    const height = mountRef.current!.clientHeight;

    // Scene and renderer setup
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    camera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);
    orthographicCamera.left = -width / cooff;
    orthographicCamera.right = width / cooff;
    orthographicCamera.top = height / cooff;
    orthographicCamera.bottom = -height / cooff;
    orthographicCamera.updateProjectionMatrix();

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(width, height);
    mountRef.current!.appendChild(renderer.domElement);

    // remove canvas
    const currentCanvas = mountRef.current!.querySelector("canvas");
    if (currentCanvas) {
      mountRef.current!.removeChild(currentCanvas);
    }
    mountRef.current!.appendChild(renderer.domElement);

    // axis
    const axesHelper = new THREE.AxesHelper(5);

    // light
    const light = new THREE.HemisphereLight(lightColor, intensity);

    // Geometry and material
    const edges = new THREE.EdgesGeometry(geometry);
    const lineMesh = new THREE.LineSegments(
      edges,
      new THREE.LineBasicMaterial({ color: lightColor })
    );
    const material = new THREE.MeshStandardMaterial({ color: materialColor });
    const mesh = new THREE.Mesh(geometry, material);

    // add scene
    const scene = new THREE.Scene();
    scene.background = new THREE.Color(sceneColor);
    scene.add(view === "perspective" ? mesh : lineMesh);
    scene.add(light);
    if (viewAxis) scene.add(axesHelper);

    const applyOrthographicCamera = (pos: number[]) => {
      orthographicCamera.position.set(pos[0], pos[1], pos[2]);
      orthographicCamera.lookAt(lineMesh.position);
      lineMesh.matrixAutoUpdate = false;
      renderer.render(scene, orthographicCamera);
    };

    // Render the scene
    const render = () => {
      if (isMouseDown) {
        mesh.rotation.x = mouseY * 0.01;
        mesh.rotation.y = mouseX * 0.01;
      }
      switch (view) {
        case "perspective":
          requestAnimationFrame(render);
          camera.aspect = width / height;
          renderer.render(scene, camera);
          break;
        case "isometric":
          applyOrthographicCamera(iPos);
          break;
        case "dimetric":
          applyOrthographicCamera(dPos);
          break;
        case "trimetric":
          applyOrthographicCamera(tPos);
          break;
        default:
          break;
      }
    };
    render();
  }, [
    geometry,
    cameraPosition.x,
    cameraPosition.y,
    cameraPosition.z,
    camera,
    orthographicCamera,
    view,
    isMouseDown,
    width,
    height,
    mouseX,
    mouseY,
    cooff,
    viewAxis,
  ]);

  // Handle mouse move events
  const handleMouseMove = (event: React.MouseEvent<HTMLDivElement>) => {
    if (isMouseDown) {
      mouseX = event.clientX - mountRef.current!.clientWidth / 2;
      mouseY = event.clientY - mountRef.current!.clientHeight / 2;
    }
  };
  const handleMouseDown = (event: React.MouseEvent<HTMLDivElement>) => {
    if (event.button === 0) {
      isMouseDown = true;
    }
  };
  const handleMouseUp = () => {
    isMouseDown = false;
  };
  const handleMouseWheel = (event: React.WheelEvent<HTMLDivElement>) => {
    camera.position.z += event.deltaY * 0.01;
  };

  // Handle object change
  const handleTetrahedronClick = () => {
    setGeometry(new THREE.TetrahedronGeometry(1));
  };

  const handleHexahedronClick = () => {
    setGeometry(new THREE.BoxGeometry(1, 1, 1));
  };

  const handleOctahedronClick = () => {
    setGeometry(new THREE.OctahedronGeometry(1));
  };

  const handleChangeView = (newView: string) => {
    setView(newView);
  };
  return {
    mountRef,
    handleMouseMove,
    handleMouseDown,
    handleMouseUp,
    handleMouseWheel,
    handleTetrahedronClick,
    handleHexahedronClick,
    handleChangeView,
    handleOctahedronClick,
    viewAxis,
    setViewAxis,
  };
};
